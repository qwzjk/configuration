" fish breaks help.
if $SHELL =~ 'fish'
	set shell=/bin/bash
endif

" Use 256 colours, because we have them
set t_Co=256

" Colour scheme
colorscheme molokai

" User interface
set number
set ruler
set linebreak
set showbreak=↳\ 
set colorcolumn=80
set cursorline
set autoindent
set smartindent
set nowrap
set laststatus=2

" Even professionals use mice sometimes!
set mouse=a

" File settings
set encoding=utf-8
setglobal fileencoding=utf-8
set tabstop=4
set shiftwidth=4
set noexpandtab
set noeol
set binary

" Type detection
filetype plugin on

" Find settings
set ignorecase
set smartcase

" Spelling
"set spell spelllang=en_ca

" Pencil digraph
digr PC 9998

" Lightning bolt digraph
digr HV 9889

" Enable backups
set backup
set backupdir=~/.local-vim/backup

" Don’t keep swap files in the same place as the file
" (It’s annoying, especially with synchronization)
set directory=~/.local-vim/tmp

" Pathogen!
call pathogen#infect()
syntax on

" Plugins!
let g:Powerline_symbols = 'fancy'

" Avoid trailing whitespace:
highlight trailingSpaces ctermbg=red
call matchadd('trailingSpaces', '\s\+$')

" Avoid leading spaces:
highlight wrongIndent ctermbg=red
call matchadd('wrongIndent', '^ \+')

" Some custom functions
function! DelCur()
	return delete(@%)
endfunction